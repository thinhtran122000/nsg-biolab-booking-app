<!DOCTYPE html>
<html>
<body>
<h1 style="color: lightblue; font-weight:bold;"> NSG BIOLAB BOOKING APP (BloC Pattern)
</h1>
<h1> Introduce </h1>
<p> This is a booking app was built by 
    <span >
        <a style="color: lightblue; font-weight:bold;" href=https://flutter.dev> Flutter
        </a>
    </span> 
    <span>framework
    </span> 
</p>
 <p align="center">
  <img src="https://nsgbio.com/wp-content/uploads/2021/08/nsgb-aioseo-logo-1.jpg" alt="Sublime's custom image">
</p> 
  <h1> Version </h1>
<ul>
  <li>
  Dart 2.18.4.
  </li>
  <li>
  Flutter version 3.3.8 - 1.1.0+1
  </li>
      
</ul>
<h1> Libraries used </h1>
<ul>
  <li style="color:lightblue;">
    <a style="color:lightblue;" href= https://pub.dev/packages/dio >Dio, Dio Interceptor
    </a>
  </li>
  <li> 
    <a style="color:lightblue;" href= https://bloclibrary.dev/#/ >Bloc
    </a>
  </li>
</ul>

<h1> APIs </h1>
<a style="color:lightblue;" href= https://nsg-bio.vinova.sg/documentation#/ >NSG Biolab APIs</a>
<p></p>

<h1> Following features </h1>

<ul>
  <li>
    <a>Authentication</a>
    <ul>
      <li>
      Login.
      </li>
       <li>
      Forgot Password.
      </li>
       <li>
      Change password.
      </li>
       <li>
      Display profile.
      </li>
       <li>
      Update profile.
      </li>
    </ul>
  </li>
    <li> 
      <a>Display bookings.
      </a>
    </li>
    <li> 
      <a>Like and Unlike booking.
      </a>
    </li>
    <li> 
        <a>Insert, Update, Delete, Search, Filter.
        </a>
    </li>
</ul>

<h1> Guilde </h1>
<h2> Prepare </h2>
<p>
 <span>Require install IDE: 
    </span> 
 <span >
        <a style="color: lightblue; font-weight:bold;" href=https://code.visualstudio.com> VS Code
        </a>
    </span> 
    <span>and
    </span> 
    <span >
        <a style="color: lightblue; font-weight:bold;" href=https://developer.android.com/studio> Android Studio Code
        </a>
    </span> 
</p>

## 1. Clone project

```sh
git clone git@gitlab.com:thinhtran122000/nsg-biolab-booking-app.git ~ Clone with SSH
```

```sh
git clone https://gitlab.com/thinhtran122000/nsg-biolab-booking-app.git ~ Clone with HTTPS
```

## 2. Setup environment variable

```sh
flutter doctor -v
```

```sh
flutter pub get
```

## 3. Run project

```sh
flutter run
```

<p> Notice: You must be start the emulator or real device to run app.</p>

<h1> App Demo </h1>
<h3> Demo details: </h3>

<ul>
  <li>
  Demo authentication.
  </li>
  <li>
  Demo insert, update, delete, search booking.
  </li>
</ul>

<h2> Demo Android: </h2>
 <video src='https://user-images.githubusercontent.com/63405663/154407934-188b087e-3107-4711-8f97-9419ec4183cc.mp4' width=180 >

</body>
</html>
